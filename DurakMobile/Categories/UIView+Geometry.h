//
//  Geometry+UIView.h
//  DurakMobile
//
//  Created by Alexandr Chernyshev on 10/18/13.
//  Copyright (c) 2013 AlexChernywev. All rights reserved.
//



@interface UIView (Geometry)
@property (assign, nonatomic) CGPoint boundsCenter;

@end
